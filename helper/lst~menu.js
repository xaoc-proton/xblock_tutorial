
exports.config = {
	multi: true,
}

exports.lst = ()=>{

	var t = 10;
	
	function helperPrepare(type, callback){
	
		var $tutorial = $('#tutorial');
		var $controls = $tutorial.find('> .content > .contentControls');

		$controls.append( $('#tutorial_helper_'+type) );
		setTimeout(function(){ window.resizeAllTextarea( $('#tutorial_helper_'+type) ), 0});
		
		callback();
	}
	
	function helperComplete(type, abort){
		
		var $tutorial = $('#tutorial');
		
		$('#tutorialHelper').append( $('#tutorial_helper_'+type) );
		
		window.setTutorialComplete(abort ? {} : {endTutorial: true, info: {
			userAgent: navigator.userAgent,
			screen: {width: window.screen.width, height: window.screen.height},
			location: decodeURI(location.href),
		}}, function(){
			location.reload();
		});
	}
	
	function helperScreenShot(type){

		var $tutorial = $('#tutorial');
		var $tutorialCanvas = $('#tutorialCanvas');
		var $formContent = $('#formContent');
		$tutorial.hide();
		$formContent.removeClass('fadeIn');
		
		var $canvas = $tutorialCanvas.find('> canvas');
		
		var signaturePad = new SignaturePad($canvas[0]);
		
		html2canvas(document.body, {canvas: $canvas[0], scale: 0.8}).then(function(){
			
			$tutorialCanvas.addClass('active');
			$tutorial.show();
			$formContent.addClass('fadeIn');
			
			var $btn = $('<button>', {text: 'Сохранить'});
			$btn.on('click', function(){
				
				var imageData = $canvas[0].toDataURL('image/png');
				$('#tutorial_helper_'+type+' .img').css('background-image', 'url('+imageData+')').addClass('active');
				$('#tutorial_helper_'+type+' .file > .el > input').val( imageData ).trigger('change');
				
				$tutorialCanvas.removeClass('active');
				$(this).remove();
			});						
			$tutorialCanvas.append($btn);
		});	
	}

	var lst = [{
		v: 'info', l: 'info',
		text: [{
			t:'Привет, чем могу помочь?',
			d:0*t,
			action: function(callback){
				
				var $tutorial = $('#tutorial');
				var links = JSON.parse($tutorial.attr('links'));
				
				for(var l in links){
					if((links[l]||{}).link === undefined) links[l] = {link: links[l]};
					if(links[l].link !== false && links[l].status !== true){
						this.controls.list.i1 = '/blocks/tutorial/static/img/star.png';
					}
				}

				if(window.userRole == 'admin') this.controls['Список доработок'] = function(){
					locationQuery({form:"tutorial/helper~list","container":"formContent"});
					window.setTutorialComplete({endTutorial: true});
				}
				
				callback();
			},
			controlsClass: 'big',
			controls: {
				exit: {t: 'Спасибо, ничего не нужно', f: function(){ 
					window.setTutorialComplete({endTutorial: true});
				}},
				list: {t: 'Покажи доступные обучения', f: function(){
					window.setTutorialComplete({type: 'tutorial'});
				}},
				'Я нашел ошибку': function(){
					window.setTutorialComplete({type: 'error'});
				},
				'Нужна доработка': function(){ 
					window.setTutorialComplete({type: 'task'});
				},
			},
		}],
		position: 'topRight',
	},{
		v: 'tutorial', l: 'tutorial',
		text: [{
			t:'Выбери любое обучение из списка, чтобы запустить его:',
			d:0*t,
			action: function(callback){
				
				var $tutorial = $('#tutorial');
				var $controls = $tutorial.find('> .content > .contentControls');
				var links = JSON.parse($tutorial.attr('links'));
				var html = '';
			
				for(var l in links){
					if((links[l]||{}).link === undefined) links[l] = {link: links[l]};
					if(links[l].link !== false){
						html += '<div class="control js-tutorial-link" link="'+l+'" force=true>'+(links[l].status!==true?'<div class="img" />':'')+(links[l].label || l)+'</div>';
					}
				}
				setTimeout(function(){ $controls.html( html ) }, 1000);
				
				callback();
			},
			controls: {
				exit: {t: 'Спасибо!', f: function(){
					window.setTutorialComplete({endTutorial: true});
				}},
			},
		}],
		position: 'topRight',
	},{
		v: 'error', l: 'error',
		action: function(callback){ helperPrepare('error', callback) },
		text: [{
			t:'Заполни форму с описанием ошибки:',
			d:0*t,
			controls: {
				screen: {t: 'Скриншот', f: function(){ helperScreenShot('error') }},
				exit: {t: 'Отправить разработчикам', f: function(){ helperComplete('error') }},
				abort: {t: 'Отмена', f: function(){ helperComplete('error', true) }},
			},
		}],
		position: 'topRight',
	},{
		v: 'task', l: 'task',
		action: function(callback){ helperPrepare('task', callback) },
		text: [{
			t:'Заполни форму с описанием задачи:',
			d:0*t,
			controls: {
				screen: {t: 'Скриншот', f: function(){ helperScreenShot('task') }},
				exit: {t: 'Отправить разработчикам', f: function(){ helperComplete('task') }},
				abort: {t: 'Отмена', f: function(){ helperComplete('task', true) }},
			},
		}],
		position: 'topRight',
	}];
}

exports.action = {
	system: true,
	prepareUser: true,
	f: function(conn, data, callback){try{

		const w = [], index = 'tutorial/helper~menu', version = 2;

		if(data.prepareUser){
			
			if((conn.user.config.prepare[index]||0)*1 < version) w.push(cb=>{try
			{
				conn.user.update(conn, Object.assign(data, {msg: [
					{n: 'config.prepare.'+index, v: version},
					{n: 'tutorial.links.tutorial/helper~menu.link', v: false},
					{n: 'tutorial.links.tutorial/helper~menu.type', v: 'static'},
				]}));
				
				cb();
				
			}catch(e){ console.log(e); cb() }});

			async.waterfall(w, ()=>{
				
				callback({status: 'ok'});
			
			});
		}else{
			
			callback({status: 'ok'});
		
		}
	}catch(e){ console.log(e); callback({status: 'err', err: 'Ошибка'}) }}
}

exports.func = {
	
	f: function(conn, data, callback){try
	{
		var w = [];
		
		if(data.msg.msg.data.type && SYS.get(data.msg.lst, 'lst.'+data.msg.msg.data.type))
		{
			conn.user.update(conn, Object.assign(data, {msg: [{n: 'tutorial.active', v: {
				t: conn.user.tutorial.active.t, s: data.msg.msg.data.type,
			}}]}));
		}else
		{
			data.msg.result.endTutorial = true;
			
			if(data.msg.msg.step == 'error' || data.msg.msg.step == 'task'){

				if(!data.msg.msg.data.info){
					
					data.msg.result.endTutorial = false;
					
					conn.user.update(conn, Object.assign(data, {msg: [{n: 'tutorial.active', v: {
						t: 'tutorial/helper~menu', s: 'info',
					}}]}));
				
				}else{
					
					w.push((cb)=>{
					
						var fields = {login: 1};
						fields['__tutorial_helper_'+data.msg.msg.step] = 1;
						
						conn.db.collection('user')
						.findOne(ObjectId(conn.user.key), fields, (err, user)=>{
							
							var id = SYS.get(user, '__tutorial_helper_'+data.msg.msg.step+'.l[0]');
							
							if(!id){ cb() }else{
								
								conn.db.collection('tmp_obj')
								.findOne(id, {}, (err, tmp_obj)=>{
									
									var obj = {_id: tmp_obj._id, col: 'tmp_obj'};

									DB.addComplex(conn, obj, {col: '__content', _id: DB.__content._id}, {
										add_time: Date.now(),
										type: data.msg.msg.step == 'error' ? 'Ошибка' : 'Задача',
										add_user: user.login,
										info: JSON.stringify(data.msg.msg.data.info),
									}, ()=>{
										
										obj.deleteLinks = true;
										obj.links = {tmp_obj: {}}
										obj.links.tmp_obj.user = false;
										obj.links.user = '__tutorial_helper_'+data.msg.msg.step;
										
										DB.deleteComplex(conn, obj, [{_id: conn.user.key, col: 'user'}], ()=>{
											cb();
										});
									});
								});
							}
						});
					});
				}
			}
		}
		
		async.waterfall(w, ()=>{ callback() });
		
	}catch(e){ console.log(e); callback() }}
}