
exports.config = {
	linkTutorial: true,
}

exports.tpl = (_, d)=>{ return [

	['div', {id: 'tutorialHelper', class: '*css*', style:()=>{/*css
		.*css* > .tutorial_helper_block {
			display: none;
		}
	css*/}}, [
	
		['script', {src:'XAOC/html2canvas/html2canvas.min.js'}],
		['script', {src:'XAOC/html2canvas/signature_pad.min.js'}],
	
		['div', {link: 'tutorial/helper~menu', class: 'tutorial-link helper-link'}],
		
		_.f({name: 'tutorial_helper', type: 'json', lst: 'tutorial/helper~menu'}),
		
		['div', {id: 'tutorial_helper_error', class: 'tutorial_helper_block'}, [
			_.c({name: 'tutorial_helper_error', col: 'tmp_obj', process: {
				links: (cb)=>{ cb({
					tutorial_helper_error: false,
					user: '__tutorial_helper_error',
				})},
				tpl: (_, d)=>{ return [
					_.html('tutorial/helper~save', _, d),
				]},
			}}),
		]],
		
		['div', {id: 'tutorial_helper_task', class: 'tutorial_helper_block'}, [
			_.c({name: 'tutorial_helper_task', col: 'tmp_obj', process: {
				links: (cb)=>{ cb({
					tutorial_helper_task: false,
					user: '__tutorial_helper_task',
				})},
				tpl: (_, d)=>{ return [
					_.html('tutorial/helper~save', _, d),
				]},
			}}),
		]],
	]],

]};

exports.script = ()=>{
	window.afterAllLoaded.push(function(){
		if($('body > .helper-link').length == 0) $('body').append( $('#tutorialHelper > .helper-link') );
	});
}

exports.style = ()=>{/*
	.helper-link {
		-webkit-animation: none!important;
		-moz-animation: none!important;
		-ms-animation: none!important;
		-o-animation: none!important;
		animation: none!important;
		height: 60px!important;
		width: 60px!important;
		top: auto!important;
		bottom: 20px!important;
		left: 20px!important;
		font-size: 0px!important;
		display: block!important;
		position: fixed!important;
		z-index: 10000!important;
	}
*/}