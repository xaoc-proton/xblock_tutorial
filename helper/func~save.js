
exports.f = function(conn, data, callback){try{
		
	var w = [];
	
	if(data.msg.data.type && SYS.get(data.lst, 'lst.'+data.msg.data.type)){
		
		conn.user.update(conn, Object.assign({}, data, {msg: [{n: 'tutorial.active', v: {
			t: conn.user.tutorial.active.t, s: data.msg.data.type,
		}}]}));
		
	}else{
		
		data.result.endTutorial = true;
		
		if(data.msg.step == 'error' || data.msg.step == 'task'){

			if(!data.msg.data.info){
				
				data.result.endTutorial = false;
				
				conn.user.update(conn, Object.assign({}, data, {msg: [{n: 'tutorial.active', v: {
					t: 'tutorial/helper~menu', s: 'info',
				}}]}));
			
			}else{
				
				w.push((cb)=>{
				
					var fields = {login: 1};
					fields['__tutorial_helper_'+data.msg.step] = 1;
					
					conn.db.collection('user')
					.findOne(ObjectId(conn.user.key), fields, (err, user)=>{
						
						var id = SYS.get(user, '__tutorial_helper_'+data.msg.step+'.l[0]');
						
						if(!id){ cb() }else{
							
							conn.db.collection('tmp_obj')
							.findOne(id, {}, (err, tmp_obj)=>{
								
								var obj = {_id: tmp_obj._id, col: 'tmp_obj'};

								DB.addComplex(conn, obj, {col: '__content', _id: DB.__content._id}, {
									add_time: Date.now(),
									type: data.msg.step == 'error' ? 'Ошибка' : 'Задача',
									add_user: user.login,
									info: JSON.stringify(data.msg.data.info),
								}, ()=>{
									
									obj.deleteLinks = true;
									obj.links = {tmp_obj: {}}
									obj.links.tmp_obj.user = false;
									obj.links.user = '__tutorial_helper_'+data.msg.step;
									
									DB.deleteComplex(conn, obj, [{_id: conn.user.key, col: 'user'}], ()=>{
										cb();
									});
								});
							});
						}
					});
				});
			}
		}
	}
	
	async.waterfall(w, ()=>{ callback() });
	
}catch(e){ console.log(e); callback() }}