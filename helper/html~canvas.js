
exports.config = {
	linkTutorial: 'tutorial/helper~menu',
}

exports.tpl = (_, d)=>{ return [

	['div', {id: 'tutorialCanvas', class: '*css*', style:()=>{/*css
		.*css* {
			display: none;
			position: fixed;
			top: 0px;
			left: 0px;
			width: 100%;
			height: 100%;
			z-index: 20000;
			overflow: auto;
		}
		.*css* > canvas {
			margin: auto;
			width: auto!important;
			height: auto!important;
			border: 4px solid #d5ad51;
		}
		.*css*.active {
			display: flex;
			align-items: center;
			justify-content: center;
		}
		.*css* > div {
			position: fixed;
			top: 0px;
			left: 0px;
			width: 100%;
			height: 100%;
			z-index: -1;
			background-image: url(/XAOC/images/clear-black-back.png);
		}
		.*css* > button {
			position: fixed;
			bottom: 30px;
			right: 30px;
			font-size: 18px;
			padding: 10px 20px;
			background-color: #d5ad51;
			border: none;
			border-radius: 4px;
			color: white;
		}
	css*/}}, [
		['canvas'],
		['div'],
	]],
]};