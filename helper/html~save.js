
exports.tpl = (_, d)=>{ return [

	['div', {}, [
		
		_.f({name: 'text', type: 'textarea'}),
		
		['div', {class: (d.file?'active':'')+' img', style: d.file ? 'background-image: url('+fs.readFileSync(PROJECT_LINK+d.file).toString()+')' : ''}],
		
		['div', {class: 'file hidden'}, [
			_.f({name: 'file', process: {
				save: (conn, field, parent, saveData, callback)=>{try{
					var link = SYS.getUploadLink(false, 'files');
					fs.writeFileSync(PROJECT_LINK+link, saveData.value);
					saveData.value = link;
					DB.saveField(conn, field, parent, saveData, callback);
				}catch(e){ console.log(e); callback({status: 'err', msg: 'Ошибка'}); }},
			}}),
		]],
		
		['div', {class: '*css*', style: ()=>{/*css
			.*css* {
				position: relative;
				display: flex;
				flex-wrap: wrap;
				padding-left: 160px;
				margin-top: 12px;
				margin-bottom: 24px;
			}
			.*css* > .complex-controls {
				left: 0px!important;
				width: 160px!important;
				border: 1px solid #d5ad51;
				color: #d5ad51;
				border-radius: 4px;
			}
			.*css* > .complex-controls > .control-add:before {
				content: 'Приложить файл'!important;
			}
			.*css* > .complex-item {
				width: 100%;
			}
			.*css* > .complex-item > .item-controls {
				display: block;
			}
			.*css* > .complex-item > .item-controls > .btn-delete {
				background-color: #d5ad51!important;
			}
		css*/}}, [
		
			_.c({name: 'files', add: 'file', process: {
				tpl: (_, d)=>{ return [
					
					['div', {controls: 'delete'}, [
						
						['div', {class: '*css*', style: ()=>{/*css
							width: 100%;
							height: 40px;
							text-align: right;
							padding: 10px;
							padding-right: 40px;	
						css*/}}, [
							
							['span', {text: SYS.get(d, 'file.n')||''}],
							
							['div', {}, [
								_.f({name: 'file', type: 'imgbtn', front: {
									afterSave: ($e)=>{ reloadItem($e) }
								}}),
							]],
						]],
					]],
				]},
			}}),
		]],
	]],
]};