
exports.access = (__, data, callback)=>SYS.requireFile('func', 'user~access').f(__, {
	allow: ['admin']
}, callback),

exports.id = (__, code, callback)=>{
	__.fields[code].col = '__content';
	__.queryIds[code] = [DB.__content._id];
	callback();
},

exports.tpl = (_, d)=>{ return [
	
	['div', {}, [
		
		_.script(()=>{	

			$('#modalEdit')
				.on('shown.bs.modal', function(e){ window.resizeAllTextarea($(e.target)) })
				.on('hidden.bs.modal', function(e){ reloadItem($("tr[editId='"+$(e.target).attr('editId')+"']")) });
			
			$(document).off('click', 'tr[editId]');
			$(document).on('click', 'tr[editId]', function(){
				var editId = $(this).attr('editId');
				var $modal = $('#modalEdit');
				reloadComplex($modal, {id: editId}, ()=>{
					$modal.attr('editId', editId);
					$modal.modal('show');
				});
			});
		}),
		
		["div",{"class": "modal fade","role": "dialog","id": "modalEdit"},[
		
			_.c({name: 'edit_form', col: 'tmp_obj', add: false, process: {
				parentDataNotRequired: true,
				id: (__, code, callback)=>{
					var field = __.fields[code];
					__.fields[code].col = 'tmp_obj';
					__.queryIds[code] = (field.filter && field.filter.id) ? [field.filter.id] : [];
					callback();
				},
				tpl: (_, d)=>{ return [
		
					["div",{"class": "modal-dialog","role": "document"},[
						
						["div",{"class": "modal-content"},[
							
							["div",{"class": "modal-header"},[
								["button",{"type": "button","class": "close","data-dismiss": "modal","aria-label": "Close"},[
									["span",{"aria-hidden": "true"},[
										["i",{"class": "fas fa-times"}]
									]]
								]],
								["h4",{"class": "modal-title"},[
									["span", {"text": "Данные сообщения"}],
								]]
							]],
							
							["div",{"class": "modal-body"},[
								["div",{"role": "form", class: 'm-t *css*', style:()=>{/*css
									.*css* .el > .select2 {
										min-width: 100%;
									}
								css*/}},[
									["div",{"class": "form-group"},[
										["label",{},[
											["span",{"text": "Статус"}]
										]],
										_.f({name: 'status', type: 'select', lst: 'tutorial/helper~status', value: ''}),
									]],
									["div",{"class": "form-group"},[
										["label",{},[
											["span",{"text": "Сообщение"}]
										]],
										_.f({name: 'text', type: 'input-'}),
									]],
									["div",{"class": "form-group"},[
										["label",{},[
											["span",{"text": "Изображение"}]
										]],
										_.if(d.file, ()=>[
											["div",{class: '*css*', style:()=>{/*css
												.*css* > img {
													width: 100%;
												}
											css*/}},[
												_.f({name: 'file', type: '*-'}),
												['img', {src: d.file ? fs.readFileSync(PROJECT_LINK+d.file).toString() : ''}],
											]],
										]),
									]],
									["div",{"class": "form-group"},[
										["label",{},[
											["span",{"text": "Файлы"}]
										]],
										['div', {class: '*css*', style: ()=>{/*css

										css*/}}, [
										
											_.c({name: 'files', add: false, process: {
												tpl: (_, d)=>{ return [
														
													['div', {class: '*css*', style: ()=>{/*css
														width: 100%;
														height: 40px;
														text-align: right;
														padding: 10px;
														padding-right: 40px;	
													css*/}}, [
														_.f({name: 'name', type: '*-'}),
														_.f({name: 'file', type: '*-'}),
														['a', {href:  SYS.get(d, 'file.l')}, [
															['span', {text:  SYS.get(d, 'file.n')||''}],
														]],
													]],
												]},
											}}),
										]],
									]],
									["div",{"class": "form-group"},[
										["label",{},[
											["span",{"text": "Мета-информация"}]
										]],
										_.f({name: 'info', type: 'input-'}),
									]],
								]]
							]],
						]]
					]]
				
				]},
			}}),
		]],

		["div",{"class": "ibox"},[
			["div",{"class": "ibox-content"},[
				["div",{"class": "bPageFilters"},[
					["div",{"class": "row"},[

					]]
				]],
				["div",{"class": "table-responsive"},[
					["table",{"class": "table table-hover table-striped table-bordered"},[
						["thead",{},[
							["tr",{},[
								["td",{},[
									["span",{"text": "Тип"}],
								]],
								["td",{},[
									["span",{"text": "Добавлен"}],
								]],
								["td",{},[
									["span",{"text": "Кто добавил"}],
								]],
								["td",{},[
									["span",{"text": "Текст сообщения"}],
								]],
								["td",{},[
									["span",{"text": "Статус"}],
								]],
							]]
						]],
						["tbody",{class: '*css*', style:()=>{/*css
							.*css* .btn-lastitem {
								position: absolute;
								right: 20px;
								bottom: 10px;
								font-size: 0px;
							}
							.*css* .btn-lastitem:after {
								content: 'Показать следующий 10 записей';
								font-size: 16px;
							}
						css*/}},[
							_.c({name: 'tmp_obj', add: false, filter: {l: -10, showOnButton: true}, process: {
								tpl: (_, d)=>{ return [
									
									["tr",{class: 'h', editId: d._id, controls: 'delete'},[
										["td",{},[
											_.f({name: 'type', type: 'text-', value: ''}),
										]],
										["td",{},[
											_.f({name: 'add_time', type: 'datetime-', value: ''}),
										]],
										["td",{},[
											_.f({name: 'add_user', type: 'text-', value: ''}),
										]],
										["td",{},[
											_.f({name: 'text', type: 'text-', value: ''}),
										]],
										["td",{},[
											_.f({name: 'status', type: 'select--', lst: 'tutorial/helper~status', value: ''}),
										]],
									]],
								]},
							}}, {tag: 'tr'}),
						]]
					]]
				]],
				["hr"],
				["div",{"class": "row m-top-lg form-inline"},[
					
				]]
			]]
		]]
	]],
]},
exports.script = ()=>{
	window.updateMenuCustom = function(info){
		info.text = 'Сообщения для разработчиков';
	};
}