

	var t = 10;
	
	function helperPrepare(type, callback){
	
		var $tutorial = $('#tutorial');
		var $controls = $tutorial.find('> .content > .contentControls');

		$controls.append( $('#tutorial_helper_'+type) );
		setTimeout(function(){ window.resizeAllTextarea( $('#tutorial_helper_'+type) ), 0});
		
		callback();
	}
	
	function helperComplete(type, abort){
		
		var $tutorial = $('#tutorial');
		
		$('#tutorialHelper').append( $('#tutorial_helper_'+type) );
		
		window.setTutorialComplete(abort ? {} : {endTutorial: true, info: {
			userAgent: navigator.userAgent,
			screen: {width: window.screen.width, height: window.screen.height},
			location: decodeURI(location.href),
		}}, function(){
			location.reload();
		});
	}
	
	function helperScreenShot(type){

		var $tutorial = $('#tutorial');
		var $tutorialCanvas = $('#tutorialCanvas');
		var $formContent = $('#formContent');
		$tutorial.hide();
		$formContent.removeClass('fadeIn');
		
		var $canvas = $tutorialCanvas.find('> canvas');
		
		var signaturePad = new SignaturePad($canvas[0]);
		
		html2canvas(document.body, {canvas: $canvas[0], scale: 0.8}).then(function(){
			
			$tutorialCanvas.addClass('active');
			$tutorial.show();
			$formContent.addClass('fadeIn');
			
			var $btn = $('<button>', {text: 'Сохранить'});
			$btn.on('click', function(){
				
				var imageData = $canvas[0].toDataURL('image/png');
				$('#tutorial_helper_'+type+' .img').css('background-image', 'url('+imageData+')').addClass('active');
				$('#tutorial_helper_'+type+' .file > .el > input').val( imageData ).trigger('change');
				
				$tutorialCanvas.removeClass('active');
				$(this).remove();
			});						
			$tutorialCanvas.append($btn);
		});	
	}

	var lst = [{
		v: 'info', l: 'info',
		text: [{
			t:'Привет, чем могу помочь?',
			d:0*t,
			action: function(callback){
				
				var $tutorial = $('#tutorial');
				var links = JSON.parse($tutorial.attr('links'));
				
				for(var l in links){
					if((links[l]||{}).link === undefined) links[l] = {link: links[l]};
					if(links[l].link !== false && links[l].status !== true){
						this.controls.list.i1 = '/blocks/tutorial/static/img/star.png';
					}
				}

				if(window.userRole == 'admin') this.controls['Список доработок'] = function(){
					locationQuery({form:"tutorial/helper~list","container":"formContent"});
					window.setTutorialComplete({endTutorial: true});
				}
				
				callback();
			},
			controlsClass: 'big',
			controls: {
				exit: {t: 'Спасибо, ничего не нужно', f: function(){ 
					window.setTutorialComplete({endTutorial: true});
				}},
				list: {t: 'Покажи доступные обучения', f: function(){
					window.setTutorialComplete({type: 'tutorial'});
				}},
				'Я нашел ошибку': function(){
					window.setTutorialComplete({type: 'error'});
				},
				'Нужна доработка': function(){ 
					window.setTutorialComplete({type: 'task'});
				},
			},
		}],
		position: 'topRight',
	},{
		v: 'tutorial', l: 'tutorial',
		text: [{
			t:'Выбери любое обучение из списка, чтобы запустить его:',
			d:0*t,
			action: function(callback){
				
				var $tutorial = $('#tutorial');
				var $controls = $tutorial.find('> .content > .contentControls');
				var links = JSON.parse($tutorial.attr('links'));
				var html = '';
			
				for(var l in links){
					if((links[l]||{}).link === undefined) links[l] = {link: links[l]};
					if(links[l].link !== false){
						html += '<div class="control js-tutorial-link" link="'+l+'" force=true>'+(links[l].status!==true?'<div class="img" />':'')+(links[l].label || l)+'</div>';
					}
				}
				setTimeout(function(){ $controls.html( html ) }, 1000);
				
				callback();
			},
			controls: {
				exit: {t: 'Спасибо!', f: function(){
					window.setTutorialComplete({endTutorial: true});
				}},
			},
		}],
		position: 'topRight',
	},{
		v: 'error', l: 'error',
		action: function(callback){ helperPrepare('error', callback) },
		text: [{
			t:'Заполни форму с описанием ошибки:',
			d:0*t,
			controls: {
				screen: {t: 'Скриншот', f: function(){ helperScreenShot('error') }},
				exit: {t: 'Отправить разработчикам', f: function(){ helperComplete('error') }},
				abort: {t: 'Отмена', f: function(){ helperComplete('error', true) }},
			},
		}],
		position: 'topRight',
	},{
		v: 'task', l: 'task',
		action: function(callback){ helperPrepare('task', callback) },
		text: [{
			t:'Заполни форму с описанием задачи:',
			d:0*t,
			controls: {
				screen: {t: 'Скриншот', f: function(){ helperScreenShot('task') }},
				exit: {t: 'Отправить разработчикам', f: function(){ helperComplete('task') }},
				abort: {t: 'Отмена', f: function(){ helperComplete('task', true) }},
			},
		}],
		position: 'topRight',
	}];
if(typeof exports != 'undefined') exports.lst = lst;
if(typeof window != 'undefined') window.LST['tutorial/helper~menu'] = lst;