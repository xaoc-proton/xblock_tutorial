
exports.install = true;

exports.f = function(conn, data, callback){
	
	switch(data.type){

		case 'createContent':
			
			async.waterfall([cb=>{
				conn.db.collection('__tutorial').insert({}, ()=>cb());
			}], ()=>callback());
			
			break;
		
		case 'prepareContent':
		default: callback(); break;
	}
}