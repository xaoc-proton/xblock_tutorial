

	var t = 10;

	var lst = [{
		v: 'hello', l: 'hello', 
		//hide: '#subFormMain',
		text: [{
			t:'Приветствую тебя в нашей системе. Тебе нужна помощь?',
			d:0*t,
			controls: {
				'Я хочу пройти обучение': function(){ window.setTutorialComplete() },
				exit: {t: 'Все понятно, я разберусь сам', f: function(){
					$('.tutorial-link').hide();
					window.setTutorialComplete({endTutorial: true});
				}},
			}
		}],
		position: 'bottomRight',
	},{
		v: 'help', l: 'help', 
		//hide: '#guiProfile',
		action: function(callback){
			//var $links = $('.tutorial-link');
			//$links.addClass('tutorial-active');
			//$links.parent().removeClass('tutorial-hide');
			callback();
		},
		text: [{
			t:'Отлично, я познакомлю тебя с основными элементами платформы.',
			d:0*t,
			controls: {
				'Продолжить': function(){ window.setTutorialComplete() },
			},
		}],
		position: 'bottomRight',
	}];
if(typeof exports != 'undefined') exports.lst = lst;
if(typeof window != 'undefined') window.LST['tutorial~hello'] = lst;