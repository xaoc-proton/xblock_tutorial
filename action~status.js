exports.f = function(conn, data, callback){try
{	
	if(data.msg.tutorial && data.msg.step)
	{
		if(!data.msg.data) data.msg.data = {};
		
		var lst = SYS.getList(conn, data.msg.tutorial);
		
		if(lst){
			
			var tutorialData = {
				msg: data.msg,
				nextStep: '',
				next: conn.user.tutorial.next || {},
				freeze: conn.user.tutorial.freeze || {},
				result: {
					endTutorial: false,
				},
				lst: lst,
			};

			if(data.msg.step === true){
				tutorialData.nextStep = lst.list.lst[0].v;
			}else{
				for(var i in lst.list.lst) if(lst.list.lst[i].v == data.msg.step && lst.list.lst[i*1+1]){
					tutorialData.nextStep = lst.list.lst[i*1+1].v;
				}
			}

			var update = [];
			
			if(data.msg.data.storageQuery) update.push({n: 'tutorial.storageQuery', v: typeof data.msg.data.storageQuery == 'object' ? JSON.stringify(data.msg.data.storageQuery) : data.msg.data.storageQuery});
			
			if(data.msg.force && (conn.user.tutorial.active||{}).t){
				update.push({n: 'tutorial.freeze', v: Object.assign({}, conn.user.tutorial.active)});
			}
			
			if(!tutorialData.nextStep || data.msg.data.endTutorial){
				
				tutorialData.result.endTutorial = true;
				
				update.push({n: 'tutorial.storageQuery', type: 'unset'});
				update.push({n: 'tutorial.links.'+data.msg.tutorial+'.status', v: true});
				
				if(tutorialData.next.t){
					update.push({n: 'tutorial.active', v: Object.assign({}, tutorialData.next)});
					update.push({n: 'tutorial.next', v: {}});
				}else{
					if(tutorialData.freeze.t){
						update.push({n: 'tutorial.active', v: Object.assign({}, tutorialData.freeze)});
						update.push({n: 'tutorial.freeze', v: {}});
					}else{
						update.push({n: 'tutorial.active', v: {}});
					}
				}
			}else{
				var active = {
					t: conn.user.tutorial.active.t,
					s: tutorialData.nextStep,
				};
				if(data.msg.step === true){
					active.t = data.msg.tutorial;
					update.push({n: 'tutorial.links.'+data.msg.tutorial+'.status', v: true});
					if(active.t == tutorialData.next.t){
						update.push({n: 'tutorial.next', v: {}});
					}
				}
				update.push({n: 'tutorial.active', v: active});
			}
			

			async.waterfall([cb=>
			{
				conn.user.update(conn, Object.assign({}, data, {msg: update}));
				data.saveAll(cb);
				data.save = []; // без этого в вызове следующего вложенного ROUTER.route данные сохранятся раньше и перетрутся
			}, cb=>
			{
				ROUTER.route(conn, Object.assign(tutorialData, {action: data.msg.tutorial, actionType: 'func'}), (res)=>{ cb(res.answer) }, true);
			}], (answer)=>
			{
				if(SYS.get(answer, 'status') == 'err')
				{
					conn.user.update(conn, Object.assign({}, data, {msg: [
						{n: 'tutorial.active', v: {}},
						{n: 'tutorial.freeze', v: {}}
					]}));
					
					callback(answer);
				}else{
					callback({status: 'ok', result: tutorialData.result});
				}
			});

		}else{ callback({status: 'err', errMsg: 'Ошибка(3)'}) }
	
	}else if(data.msg.link){
		
		data.msg.tutorial = data.msg.link;
		data.msg.step = true;
		delete data.msg.link;
		
		ROUTER.route(conn, data.msg, (res)=>{ callback(res.answer) }, true);
		
	}else{ callback({status: 'err', errMsg: 'Ошибка(2)'}) }
	
}catch(e){ console.log(e); callback({status: 'err', errMsg: 'Ошибка(1)'}); }}