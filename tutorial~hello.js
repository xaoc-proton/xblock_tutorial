
exports.config = {
	multi: true,
}

exports.lst = ()=>{

	var t = 10;

	var lst = [{
		v: 'hello', l: 'hello', 
		//hide: '#subFormMain',
		text: [{
			t:'Приветствую тебя в нашей системе. Тебе нужна помощь?',
			d:0*t,
			controls: {
				'Я хочу пройти обучение': function(){ window.setTutorialComplete() },
				exit: {t: 'Все понятно, я разберусь сам', f: function(){
					$('.tutorial-link').hide();
					window.setTutorialComplete({endTutorial: true});
				}},
			}
		}],
		position: 'bottomRight',
	},{
		v: 'help', l: 'help', 
		//hide: '#guiProfile',
		action: function(callback){
			//var $links = $('.tutorial-link');
			//$links.addClass('tutorial-active');
			//$links.parent().removeClass('tutorial-hide');
			callback();
		},
		text: [{
			t:'Отлично, я познакомлю тебя с основными элементами платформы.',
			d:0*t,
			controls: {
				'Продолжить': function(){ window.setTutorialComplete() },
			},
		}],
		position: 'bottomRight',
	}];
}

exports.func = {
	
	f: function(conn, data, callback){try{
	
		if(data.msg.step == 'hello' && data.msg.data.endTutorial){
			
			conn.db.collection('user').findOne(ObjectId(conn.user.key), {tutorial: 1}, (err, user)=>{
				
				var links = SYS.get(user, 'tutorial.links') || {}, update = [];

				for(var l in links) if(links[l].status !== true && links[l].link !== false){
					update.push({n: 'tutorial.links.'+l+'.link', v: true});
				}

				conn.user.update(conn, Object.assign(data, {msg: update}));
				
				callback();
			});
		}else{
			callback();
		}
	}catch(e){ console.log(e); callback() }}
}