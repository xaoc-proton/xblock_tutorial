exports.id = (__, code, callback)=>{
	__.fields[code].col = 'user';
	__.queryIds[code] = [__.user.key];
	callback();
}
exports.tpl = (_, d)=>{
	
	if(!d.tutorial) d.tutorial = {};
	if(!d.tutorial.active) d.tutorial.active = {};
	
	// базовая запись туториала юзера в сессию
	if(!_.__.pre && !SYS.get(_.__.user, 'tutorial.loaded')){
		_.__.user.tutorial = d.tutorial;
		_.__.user.tutorial.loaded = true;
	}
	
	var currentTutorial = d.tutorial.active.t || '',
		currentStep = d.tutorial.active.s || '',
		nextTutorial;
	
	if(!currentStep && currentTutorial){
		var l = SYS.getList(_.__, currentTutorial);
		if(l) currentStep = l.defval;
	}
	if(!currentTutorial && (d.tutorial.next||{}).t) nextTutorial = d.tutorial.next.t;
	
return [

	['script', {src:'XAOC/typed/typed.min.js'}],
	
	['div', {
		id: 'tutorial', 
		links: JSON.stringify(d.tutorial.links||{}), 
		storageQuery: d.tutorial.storageQuery||'', 
		currentTutorial: currentTutorial, currentStep: currentStep, nextTutorial: nextTutorial, 
	}, [
			
		_.f({name: 'tutorial', type: 'json'}),
		
		((_, d)=>_.__.pre ? [] : [
			_.f({name: 'currentTutorial', type: 'json', lst: currentTutorial}),
		])(_, d),
	
		['img', {class: 'guru'}],
		
		['div', {class: 'content'}, [
			['div', {class: 'img'}],
			['div', {class: 'text'}],
			['div', {class: 'contentControls'}],
		]],
		
		Object.keys(BLOCK.html)
			.filter(_=>BLOCK.html[_].linkTutorial)
			.map(key=>[
				_.if(BLOCK.html[key].linkTutorial === true || BLOCK.html[key].linkTutorial == currentTutorial, ()=>[
					_.html(key, _, Object.assign({currentTutorial: currentTutorial}, d)),
				]),
			]),

	]],
	
]}

exports.func = ()=>{
	
	window.setTutorial = function(data, after, noaction){

		if(noaction !== true){
			
			var $body = $('body');
			
			if($body.attr('tutorial-hide')) $($body.attr('tutorial-hide')).removeClass('tutorial-hide');
			if($body.attr('tutorial-active')) $($body.attr('tutorial-active')).removeClass('tutorial-active');

			if(data.hide){
				$(data.hide).addClass('tutorial-hide');
				$body.attr('tutorial-hide', data.hide);
			}
			
			(!data.check ? function(check, callback){ callback() } : function(check, callback){
				if(!$(check.search).length){
					window.checkTutorialBlock(check.fail, callback);
				}else{
					callback();
				}
			})(data.check, function(){
				if(typeof data.action != 'function') data.action = function(cb){ cb() };
				data.action(function(next){
					if(data.active){
						$(data.active).addClass('tutorial-active');
						$body.attr('tutorial-active', data.active);
					}
					if(next !== false) window.setTutorial(data, after, true);
				});				
			});
		}else{
		
			var $t = $('#tutorial');
			$t.addClass('active');
				
			if(data.position) $('#subFormTutorial').attr('tutorialPosition', data.position);

			data.src = '/static/img/guru.jpg';
			if(data.src) $t.find('> .guru').attr('src', data.src);
			
			if(data.t){
				
				var p = {strings: [ data.t+'^'+(data.d||0) ]};

				p.onComplete = after.length > 0 ? function(){
					var nextStep = after.shift();
					window.setTutorial(nextStep, after);
				} : function(){
					if(data.controls){
						
						var $controls = $('<div />', {class: 'controls '+(data.controlsClass||'')});
						for(var c in data.controls){
							
							let $btn = $('<button />');
							let $text = $('<span />');
							$btn.append( $text );
							
							let text, icon1, icon2, onclick;
							text = c;

							if(data.controls[c] && data.controls[c] !== false){
								
								$btn.addClass('active');
								
								if(typeof data.controls[c] == 'function'){
									
									onclick = data.controls[c];
								
								}else{
									
									onclick = data.controls[c].f;
									if(data.controls[c].t) text = data.controls[c].t;
									if(data.controls[c].i1) icon1 = data.controls[c].i1;
									if(data.controls[c].i2) icon2 = data.controls[c].i2;
									
									if(c == 'exit' && !data.controls[c].i2) icon2 = '/blocks/tutorial/static/img/exit.png';
								}
							}
							
							$text.text( text );
							if(onclick) $btn.on('click', onclick);
							if(icon1) $btn.prepend( $('<div />', {class: 'img', style: 'background-image: url('+icon1+')'}) );
							if(icon2) $btn.append( $('<div />', {class: 'img', style: 'background-image: url('+icon2+')'}) );

							$controls.append( $btn );
						}
						$('#tutorial').append( $controls ).addClass('has-controls');
					}
					if(typeof data.onComplete == 'function') data.onComplete();
				}
				
				if(window.tutorialTyped) window.tutorialTyped.destroy();
				window.tutorialTyped = new Typed('#tutorial > .content > .text', p);
				//$('#tutorial > .content > .contentControls').html();
				
				if(data.d > 0){ // таймауты используются при автоматическом переключении шагов внутри одного блока
					setTimeout(function(){
						if(data.hide) $(data.hide).removeClass('tutorial-hide');
						if(data.active) $(data.active).removeClass('tutorial-active');
					}, data.d);
				}
				
				var $img = $('#tutorial > .content > .img');
				if(data.i){
					$img.css('background-image', 'url('+data.i+')');
					$img.addClass('active');
				}else{
					$img.removeClass('active');
				}

			}else{
				var nextStep = after.shift();
				window.setTutorial(nextStep, after);
			}
		}
	}
	
	window.setTutorialComplete = function(data, callback){
		
		var $t = $('#tutorial');
		var currentTutorial = $t.attr('currentTutorial');
		var currentStep = $t.attr('currentStep');
		
		if(window.oLST[currentTutorial]){
		
			if(data && data.storageQuery === true) data.storageQuery = history.state.subform;
			
			wsSendCallback( {action: 'tutorial~status', tutorial: currentTutorial, step: currentStep, data: data}, function(data){

				if(data && data.result && data.result.endTutorial){
					$('.tutorial-hide, .tutorial-active, .tutorial-disabled')
						.removeClass('tutorial-hide')
						.removeClass('tutorial-active')
						.removeClass('tutorial-disabled');
				}
				
				$t.addClass('hidden');
				
				if(typeof callback == 'function'){
					callback(data);
				}else{
					locationQuery({form: 'tutorial~main', container: 'subFormTutorial', history: false});
				}
			});
		}
	}
	
	window.setTutorialOnLoad = function(){try{

		function checkCurrentTutorial(){
			
			var $t = $('#tutorial');
			var currentTutorial = $t.attr('currentTutorial');
			var currentStep = $t.attr('currentStep');
			var nextTutorial = $t.attr('nextTutorial');

			if(nextTutorial){
				wsSendCallback({action: 'tutorial~status', link: nextTutorial}, function(result){
					locationQuery({form: 'tutorial~main', container: 'subFormTutorial', history: false});
				});
			}else{
				
				var links = JSON.parse($t.attr('links'));
				
				for(var l in links){

					if((links[l]||{}).link === undefined) links[l] = {link: links[l]};

					if(links[l].link === true)
					{ // тут ссылки без привязки, но попадающие в общий список
					}else if(links[l].link)
					{ // тут активные ссылки (еще не нажималась)
						if(links[l].status !== true){
							var $e = $(links[l].link);
							if($e && $e.find('> .tutorial-link').length == 0) $e.append($('<div >', {class: 'tutorial-link', text: '?', link: l}));
						}
					}else
					{ // тут пассивные ссылки (уже нажимались, но видны в общем списке)
					
					}
				}
				
				if(currentTutorial && currentStep){
					
					var lst = window.oLST[currentTutorial];
					
					if(lst){
						
						if(!lst[currentStep]) currentStep = Object.keys(lst)[0];
							
						window.setTutorial(lst[currentStep], lst[currentStep].text.concat());
					}else{
						// это может не работать с нестандартными роутингами
						currentTutorial = currentTutorial.split('~');
						formResAfterLoad['/blocks/'+currentTutorial[0]+'/lst/'+currentTutorial[1]+'.js'] = [function(){ checkCurrentTutorial() }];
					}
				}
			}
		}
	
		if($('#tutorial').length){
			checkCurrentTutorial();
		}else{
			waitForLoadElementByCode[$('#subFormTutorial').attr('code')] = function(){
				delete waitForLoadElementByCode[$('#subFormTutorial').attr('code')];
				checkCurrentTutorial();
			}
		}
	}catch(e){ console.log('window.setTutorialOnLoad error', e) }}
	
	window.afterAllLoaded.push( window.setTutorialOnLoad );
	
	$(document).off('click', '.tutorial-link, .js-tutorial-link');
	$(document).on('click', '.tutorial-link, .js-tutorial-link', function(e){
		
		var $link = $(this);
		var $tutorial = $('#tutorial');
		var link = $link.attr('link');
		var force = $link.attr('force');
		
		if(link){
			
			var currentTutorial = $('#tutorial').attr('currentTutorial');
			
			if(!force && currentTutorial){
					
				$.notify('Необходимо закончить текущее обучение');
			
			}else{
				
				wsSendCallback({action: 'tutorial~status', link: link, force: force}, function(result){
					$link.addClass('hidden');
					locationQuery({form: 'tutorial~main', container: 'subFormTutorial', history: false});
				});
			}
		}
	});
	
	// запускается, если прошла проверка, что туториал находится на неправильной странице
	window.checkTutorialBlock = function(data, callback){try{
		
		var storageQuery = JSON.parse( $('#tutorial').attr('storageQuery') );
		var code = $(data.form).attr('code');

		if(storageQuery && typeof storageQuery == 'object' && code){
			waitForLoadElementByCode[code] = function(){
				delete waitForLoadElementByCode[code];
				window.afterAllLoaded.push(callback);
			}
			locationQuery( storageQuery );
		}else{
			callback(false);
			window.setTutorialComplete(data.err||{});			
		}
	}catch(e){
		callback(false);
		window.setTutorialComplete(data.err||{});
	}}
}

exports.style = ()=>{/*
	
	#subFormTutorial {
		position: fixed;
		z-index: 10000;
	}
	body.isMobile #subFormTutorial {
		max-width: 100%;
	}
	#subFormTutorial > #tutorial {
		width: 600px;
	}
	body.isMobile #subFormTutorial > #tutorial {
		max-width: 100%;
	}
	#subFormTutorial[tutorialPosition=topLeft] {
		top: 0px;
		left: 0px;
	}
	#subFormTutorial[tutorialPosition=bottomLeft] {
		bottom: 0px;
		left: 0px;
	}
	#subFormTutorial[tutorialPosition=bottomRight] {
		bottom: 0px;
		right: 0px;
	}
	#subFormTutorial[tutorialPosition=topRight] {
		top: 0px;
		right: 0px;
	}
	#subFormTutorial[tutorialPosition=topLeft] > #tutorial > .guru {
		left: 10px;
		right: initial;
	}
	#subFormTutorial[tutorialPosition=bottomLeft] > #tutorial > .guru {
		left: 10px;
		right: initial;
	}
	#subFormTutorial[tutorialPosition=topLeft] > #tutorial > .content {
		padding-left: 60px;
		padding-right: 10px;
	}
	#subFormTutorial[tutorialPosition=bottomLeft] > #tutorial > .content {
		padding-left: 60px;
		padding-right: 10px;
	}
	body.isMobile #subFormTutorial[tutorialPosition=bottomLeft] > #tutorial > .content {
		padding-left: 40px;
	}

	.tutorial-hide {
		display: none!important;
	}
	
	body[tutorial-active]:after {
		content: '';
		position: fixed!important;
		z-index: 9999;
		width: 100%;
		height: 100%;
		top: 0px;
		left: 0px;
		background-image: url(/XAOC/images/clear-grey-back.png);
	}

	.tutorial-active {
		z-index: 10000;
		position: relative;
		box-shadow: 0 0 10px 20px #ffffff;
	}
	
	.tutorial-link {
		position: absolute;
		top: 0px;
		right: 0px;
		background-image: url(/static/img/guru.jpg);
		width: 60px;
		height: 60px;
		background-size: contain;
		background-position: center;
		background-repeat: no-repeat;
		border-radius: 50%;
		border: 3px solid #d5ad51;
		font-size: 80px;
		line-height: 56px;
		color: #d5ad51;
		text-align: center;
		cursor: pointer;
		z-index: 1;
	}

	body.isMobile .tutorial-link {
		width: 40px;
		height: 40px;
		line-height: 38px;
		font-size: 32px;
	}

	.tutorial-link:hover {
		opacity: 0.5;
	}

	.tutorial-link {
		-webkit-animation: scale 1s infinite alternate;
		-moz-animation: scale 1s infinite alternate;
		-ms-animation: scale 1s infinite alternate;
		-o-animation: scale 1s infinite alternate;
		animation: scale 1s infinite alternate;
	}

	@-webkit-keyframes scale { from { transform: scale(1) } to { transform: scale(0.5) }}
	@-moz-keyframes scale { from { transform: scale(1) } to { transform: scale(0.8) }}
	@-ms-keyframes scale { from { transform: scale(1) } to { transform: scale(0.8) }}
	@-o-keyframes scale { from { transform: scale(1) } to { transform: scale(0.8) }}
	@keyframes scale { from { transform: scale(1) } to { transform: scale(0.8) }}

	.tutorial-link.tutorial-active {

		-webkit-animation: tutorial_link_active 1s infinite alternate;
		-moz-animation: tutorial_link_active 1s infinite alternate;
		-ms-animation: tutorial_link_active 1s infinite alternate;
		-o-animation: tutorial_link_active 1s infinite alternate;
		animation: tutorial_link_active 1s infinite alternate;
	}

	@-webkit-keyframes tutorial_active { from { background-color: transparent; } to { background-color: #555; }}
	@-moz-keyframes tutorial_active { from { background-color: transparent; } to { background-color: #555; }}
	@-ms-keyframes tutorial_active { from { background-color: transparent; } to { background-color: #555; }}
	@-o-keyframes tutorial_active { from { background-color: transparent; } to { background-color: #555; }}
	@keyframes tutorial_active { from { background-color: transparent; } to { background-color: #555; }}

	@-webkit-keyframes tutorial_gui_active { from { border-color: black; } to { border-color: white; }}
	@-moz-keyframes tutorial_gui_active { from { border-color: black; } to { border-color: white; }}
	@-ms-keyframes tutorial_gui_active { from { border-color: black; } to { border-color: white; }}
	@-o-keyframes tutorial_gui_active { from { border-color: black; } to { border-color: white; }}
	@keyframes tutorial_gui_active { from { border-color: black; } to { border-color: white; }}

	@-webkit-keyframes tutorial_link_active {
		from { border-color: #d5ad51; color: #d5ad51; }
		to { border-color: white; color: white; }
	}
	@-moz-keyframes tutorial_link_active {
		from { border-color: #d5ad51; color: #d5ad51; }
		to { border-color: white; color: white; }
	}
	@-ms-keyframes tutorial_link_active {
		from { border-color: #d5ad51; color: #d5ad51; }
		to { border-color: white; color: white; }
	}
	@-o-keyframes tutorial_link_active {
		from { border-color: #d5ad51; color: #d5ad51; }
		to { border-color: white; color: white; }
	}
	@keyframes tutorial_link_active {
		from { border-color: #d5ad51; color: #d5ad51; }
		to { border-color: white; color: white; }
	}
	
	#tutorial {
		position: relative;
		width: 100%;
		font-size: 14px;
		display: none;
	}
	#tutorial.active[currentTutorial] {
		display: flex;
	}
	
	#tutorial > .controls {
		position: absolute;
		bottom: 6px;
		left: 0px;
		width: 100%;
		display: flex;
		justify-content: center;
	}
	#tutorial > .controls.big {
		top: 100%;
		flex-wrap: wrap;
		margin-top: -40px;
	}
	#tutorial > .controls > button {
		color: #888;
		border: 2px solid #888;
		background-image: url(/XAOC/images/clear-black-back.png);
		padding: 10px 20px;
		display: flex;
		justify-content: space-evenly;
	}
	#tutorial > .controls.big > button {
		width: 60%;
	}
	#tutorial > .controls > button.active {
		border-color: #d5ad51;
		color: #d5ad51;
	}
	#tutorial > .controls > button.active:hover {
		color: white;
	}
	#tutorial > .controls > button > .img{
		background-size: contain;
		background-repeat: no-repeat;
		background-position: center;
		margin: 0px 10px;
		height: 20px;
		width: 20px;
	}
	#tutorial > .content > .contentControls {
		margin-top: 10px;
	}
	#tutorial > .content > .contentControls > .control {
		display: flex;
		cursor: pointer;
		padding: 4px 20px;
	}
	#tutorial > .content > .contentControls > .control:hover {
		color: white;
	}
	#tutorial > .content > .contentControls > .control > .img {
		background-image: url(/blocks/tutorial/static/img/star.png);
		background-size: contain;
		background-repeat: no-repeat;
		background-position: center;
		margin: 0px 10px;
		height: 20px;
		width: 20px;			
	}
	
	#tutorial > .content > .contentControls textarea {
		background: transparent;
		border-color: #d8b35f;
		padding: 10px;
		width: 100%;
		color: #ddd;
	}
	#tutorial > .content > .contentControls textarea:focus {
		outline: none;
	}
	#tutorial > .content > .contentControls .img {
		background-size: cover;
		width: 100%;
	}
	#tutorial > .content > .contentControls .img.active {
		height: 300px;
		border: 2px solid #9f8750;
	}
	
	#tutorial > .guru {
		position: absolute;
		border-radius: 50%;
		border: 3px solid #d5ad51;
		right: 10px;
		top: 10px;
		width: 64px;
	}
	body.isMobileSmall #tutorial > .guru {
		right: 10px;
		top: 10px;
		width: 64px;
	}
	
	#tutorial > .content {
		width: 100%;
		margin: 30px;
		min-height: 100px;
		border: 2px solid #d5ad51;
		background-image: url(/XAOC/images/clear-black-back.png);
		padding: 20px;
		padding-right: 60px;
		white-space: pre-wrap;
		color: #d5ad51;
		overflow: hidden;
		display: flex;
		flex-wrap: wrap;
	}
	body.isMobile #tutorial > .content {
		min-height: 0px;
	}
	#tutorial.has-controls > .content {
		padding-bottom: 30px;
	}
	body.isMobileSmall #tutorial > .content {
		font-size: 12px;
		padding-right: 40px;
	}
	#tutorial > .content > .typed-cursor {
		display: none!important;
	}
	#tutorial > .content > .text, 
	#tutorial > .content > .contentControls {
		width: 100%;
	}
	#tutorial > .content > .img {
		width: 60px;
		background-size: contain;
		flex-shrink: 0;
		background-repeat: no-repeat;
		background-position: center;
		margin-right: 20px;
		display: none;				
	}
	body.isMobileSmall #tutorial > .content > .img {
		width: 40px;
	}
	#tutorial > .content > .img.active {
		display: block;
	}
*/}