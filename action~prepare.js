
exports.system = true;
exports.prepareUser = true;
exports.customLocal = true;
exports.info = 'Подготовка блока обучения (pid='+process.pid+')';
exports.zIndex = 1000;

exports.f = function(conn, data, callback){try{

	if(data.prepareUser)
	{
		if(!conn.user.tutorial) conn.user.tutorial = {};
		
		const w = [], index = 'add_tutorial', version = 6;
	
		if((conn.user.config.prepare[index]||0)*1 < version) w.push(cb=>{try
		{
			conn.user.update(conn, Object.assign(data, {msg: [
				{n: 'config.tutorial', v: DB.__tutorial._id || ''},
				{n: 'config.prepare.add_tutorial', v: version},
				//{n: 'tutorial.active.t', v: 'tutorial~hello'},
				//{n: 'tutorial.links.tutorial~hello.link', v: false}
			]}));
			
			cb();
		}catch(e){ console.log(e); cb() }});
		
		async.waterfall(w, ()=>
		{
			callback({status: 'ok'});
		});
	
	}else
	{
		conn.db.collection('__tutorial').findOne({}, {}, (err, __tutorial)=>
		{
			DB.__tutorial = __tutorial || {};
			callback({status: 'ok'});
		});
	}
}catch(e){ console.log(e); callback({status: 'err', err: 'Ошибка подготовки обучения для пользователя'}) }}